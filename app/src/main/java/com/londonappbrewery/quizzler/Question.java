package com.londonappbrewery.quizzler;

/**
 * Created by Draklob on 03/02/2018.
 */

public class Question {

    private int mQuestionID;
    private boolean mAnswer;

    public Question(int questionID, boolean answer) {
        mQuestionID = questionID;
        mAnswer = answer;
    }

    public int getQuestionID() {
        return mQuestionID;
    }

    public void setQuestionID(int questionID) {
        mQuestionID = questionID;
    }

    public boolean isAnswer() {
        return mAnswer;
    }

    public void setAnswer(boolean answer) {
        mAnswer = answer;
    }
}
