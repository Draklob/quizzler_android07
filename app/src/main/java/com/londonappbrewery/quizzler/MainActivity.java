package com.londonappbrewery.quizzler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    // TODO: Declare constants here
    final int PROGRESS_BAR_INCREMENT = 5;

    // TODO: Declare member variables here:
    Button mTrueButtton;
    Button mFalseButton;
    TextView mQuestionText;
    TextView mScore;
    ProgressBar mProgressBar;
    int mIndex;
    int mScoreValue;


    // TODO: Uncomment to create question bank
    private Question[] mQuestionBank = new Question[] {
            new Question(R.string.question_1, true),
            new Question(R.string.question_2, true),
            new Question(R.string.question_3, true),
            new Question(R.string.question_4, true),
            new Question(R.string.question_5, true),
            new Question(R.string.question_6, false),
            new Question(R.string.question_7, true),
            new Question(R.string.question_8, false),
            new Question(R.string.question_9, true),
            new Question(R.string.question_10, true),
            new Question(R.string.question_11, false),
            new Question(R.string.question_12, false),
            new Question(R.string.question_13,true)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mScoreValue = savedInstanceState.getInt("SCORE_KEY");
            mIndex = savedInstanceState.getInt("INDEX_KEY");
        } else {
            mScoreValue = 0;
            mIndex = 0;
        }

        mTrueButtton = (Button) findViewById(R.id.true_button);
        mFalseButton = (Button) findViewById(R.id.false_button);
        mQuestionText = (TextView) findViewById(R.id.question_text_view);
        mScore = (TextView) findViewById(R.id.score);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.setMax( mQuestionBank.length);

        mQuestionText.setText(mQuestionBank[mIndex].getQuestionID());
        mScore.setText("Score " + mScoreValue + "/13");

        mTrueButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(true);
                updateQuestion();
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(false);

                updateQuestion();
            }
        });
    }

    void updateQuestion() {
        mIndex = (mIndex + 1) % mQuestionBank.length;

        if (mIndex == 0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Game Over");
            alert.setCancelable(false);
            alert.setMessage("You scored " + mScoreValue + " points!");
            alert.setPositiveButton("Close Application", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            alert.show();
        }

        mQuestionText.setText(mQuestionBank[mIndex].getQuestionID());
    }

    void checkAnswer(boolean userSelection) {
        boolean correctAnswer = mQuestionBank[mIndex].isAnswer();

        if (correctAnswer == userSelection) {
            Toast.makeText(this, R.string.correct_toast, Toast.LENGTH_SHORT).show();
            updateScore();
        } else {
            Toast.makeText(this, R.string.incorrect_toast, Toast.LENGTH_SHORT).show();

        }
    }

    void updateScore()
    {
        mScoreValue++;
        String score = "Score " + mScoreValue + "/13";
        mScore.setText(score);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mProgressBar.setProgress(mScoreValue, true);
        } else {
            mProgressBar.setProgress(mScoreValue);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("SCORE_KEY", mScoreValue);
        outState.putInt("INDEX_KEY", mIndex);
    }
}
